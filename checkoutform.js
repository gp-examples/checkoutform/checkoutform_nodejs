
    // Projects: Example | Checkout example in nodejs using greenpay form

    // File description: Checkout flow to pay request orders with greenpy form.

    // Authors: mumana@greenpay.me, aviquez@greenpay.me

    // Versions:
    //     2018-08-10 :   Firts release @greenpay/checkput_nodejs.


// Required libs

//Import enviroment var's

//See .env.default and configure it with the credentials provided by Greenpay support team
require('dotenv').config({path: 'C:/Users/andre/'+"Google Drive"+'/Repos/GitLab/gp_examples/.env'});

const unirest = require("unirest");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/**
 *  Creates an order to pay in greenpay system
 */
function postOrder(data, accessToken) {
    return new Promise(function (resolve, reject) {
        unirest.post(process.env.MERCHANT_ENDPOINT)
            .headers({
                "Accept": "application/json",
                "Content-Type": "application/json"
            })
            .send(data)
            .end(function (response) {
                if (response.status === 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            });
    });
}
/**
 * Data example needed to creates the order in greenpay system
 */
const order = {
    "secret": process.env.SECRET,
    "merchantId": process.env.MERCHANT_ID,
    "terminal": process.env.TERMINAL_ID,
    "amount": 1,
    "currency": process.env.CURRENCY,
    "desciption": "desc",
    "orderReference": "1",
    "callback": "http://localhost:3000/callback" //If you have a callback setted up in Greenpay Gateway, if you send it its will be overwrite
};

/**
 * Main function
 */
app.get("/order", async function (req, res) {
    try {
        const security = await postOrder(order);
        console.log(security);
        console.log(process.env.CHECKOUT_FORM_ENDPOINT+"/" + security.session);

        res.redirect(process.env.CHECKOUT_FORM_ENDPOINT+"/"+security.session);
    } catch (err) {
        res.send(err);
    }
});


app.get("/callback/:data", function (req,res) {
    
    //get the response in bse64
    const data = req.params.data;
    //decode the response in base64 and converts to Json 
    var responsejson = Buffer.from(data,'base64').toString();

    console.log(responsejson);
    res.send(JSON.parse(responsejson));
});

app.listen(3000, () => console.log('app listening on port 3000!'));

